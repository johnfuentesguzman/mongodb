const path = require('path');

const express = require('express');
const bodyParser = require('body-parser');

const errorController = require('./controllers/error');
const mongoConnect = require('./util/database').mongoConnect; // gettig the setup method for mongo


const User = require('./models/user'); // user model
const app = express();

app.set('view engine', 'ejs');
app.set('views', 'views');

/*** Routes */
const adminRoutes = require('./routes/admin');
const shopRoutes = require('./routes/shop');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use((req, res, next) => {
  // getting the user from 'user collection´and setting it as part of the request parameters 'user'
  User.findById('6000ced55674a5e3ebe70fb7')
    .then(user => {
      req.user = new User(user.name, user.email, user.cart, user._id );
      next();
    })
    .catch(err => console.log(err));
});

app.use('/admin', adminRoutes);
app.use(shopRoutes);

app.use(errorController.get404);
 
mongoConnect( () => { // getting the mongo setup/method promisse
  app.listen(3000);
});
