const getDb = require('../util/database').getDb;
const mongoDb = require('mongodb');

class Product {
  constructor(title, price, description, imageUrl, id, userId) {
    this.title = title;
    this.price = price;
    this.description = description;
    this.imageUrl = imageUrl;
    this._id = id ? new mongodb.ObjectId(id) : null;
    this.userId = userId;
  }

  save() {
    const db = getDb();
    let dbOp;
    if(this._id){ // ig there is _id === i wanna update the data
        // get if the id exist on DB and then update the values which have changed
        dbOp = db.collection('products').updateOne({_id: new mongoDb.ObjectID(this._id)}, { $set: this });
    } else { // if there not _id === i wanna add data to DB

      dbOp = db.collection('products').insertOne(this);
    }
 
    return dbOp
      .then(result => {
        console.log(result);
      })
      .catch(err => {
        console.log(err);
      });
  }

  static fetchAll() {
    const db = getDb();
    return db
      .collection('products')
      .find()
      .toArray()
      .then(products => {
        console.log(products);
        return products;
      })
      .catch(err => {
        console.log(err);
      });
  }

  static findById(productId){
    const db = getDb();
    return db.collection('products')
    .find({_id: new mongoDb.ObjectID(productId) }) // to make the comparison, due to monto db save the _id as a object not a string/number
    .next() // to get all the documents available with this id in this collection (this case i know just would be 1 DATA)
    .then((product) => {
      return product;
    })
    .catch((error) => {
      console.error(error);
    });
  }
  
  static deleteById(prodId) {
    const db = getDb();
    return db
      .collection('products')
      .deleteOne({ _id: new mongoDb.ObjectId(prodId) })
      .then(result => {
        console.log('Deleted');
      })
      .catch(err => {
        console.log(err);
      });
  }

}


module.exports = Product;
