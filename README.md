# README #
node app using Mongo DB

### What will I learn? ###

* Mongo setup (app.js)
* Mongo compass
* Installing node mongo DB Driver 
* CRUD using mongo DB ATLAS  

### Installation###
* npm i --save mongodb

### Documentation ###
* Mongo db commands for crud operations https://docs.mongodb.com/manual/crud/
* DB on mongo clusted used  https://cloud.mongodb.com/v2/5fff7c53f6023d79a8d75dc8#clusters 