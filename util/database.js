const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;

let _db;


const mongoConnect = callback => {
  MongoClient.connect(
    'mongodb+srv://johnfuentesguzman:kurtco235@cluster0.4dsni.mongodb.net/shopStore?retryWrites=true&w=majority'
  )
    .then(client => {
      console.log('Connected!');
      _db = client.db(); // saving the connection on the global variable to avoid make one connection every single time it is needed
      callback();
    })
    .catch(err => {
      console.log(err);
    });
};

const getDb = () => {
  if (_db) {
    return _db;
  }
  throw 'No database found!';
};

// exporting methods
exports.mongoConnect = mongoConnect;
exports.getDb = getDb;
